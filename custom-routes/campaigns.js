const fs  = require('fs');
const findIndex = require("lodash.findindex");

const dataFromDb = JSON.parse(fs.readFileSync("db.json").toString());

const getCampaigns = (req, res) => {
    const criteria = req.query.criteria;
    const inactive = req.query.inactive;

    let campaigns = [...dataFromDb.campaigns];

    if(criteria) {
        campaigns = campaigns.filter(campaign => {
            const reg = new RegExp(criteria, "i");
            return campaign.title.search(reg) != -1;
        });
    }

    if(inactive !== 'true') {
        campaigns = campaigns.filter(campaign => {
            return campaign.active === true;
        })
    }

    res.send({
        items: campaigns
    });

}

module.exports = { getCampaigns }