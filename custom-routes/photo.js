const uuid = require('uuid/v1');

const postPhoto = (req, res, next) => {
  if (req.method === 'POST') {
    req.body.fileName = `${uuid()}.png`;
  }
  next()
};

module.exports = { postPhoto };
