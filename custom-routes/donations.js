const fs  = require('fs');
const findIndex = require("lodash.findindex");

const dataFromDb = JSON.parse(fs.readFileSync("db.json").toString());

const getDonations = (req, res) => {
  const {
    startId,
    count,
    amountMin,
    amountMax,
    dateStart,
    dateEnd,
    name
  } = req.query;

  let donationsRequested = [...dataFromDb.donations];

  if (name) {
    donationsRequested = donationsRequested.filter(donation => {
      const reg = new RegExp(name, "i");
      return donation.user_name.search(reg) != -1;
    });
  }

  if (amountMin) {
    donationsRequested = donationsRequested.filter(donation => {
      return donation.amount >= amountMin;
    });
  }

  if (amountMax) {
    donationsRequested = donationsRequested.filter(donation => {
      return donation.amount <= amountMax;
    });
  }

  if (dateStart) {
    donationsRequested = donationsRequested.filter(donation => {
      return donation.date >= dateStart;
    });
  }

  if (dateEnd) {
    donationsRequested = donationsRequested.filter(donation => {
      return donation.date <= dateEnd;
    });
  }

  const limit = parseInt(count) || 100;
  let nextId = donationsRequested[limit] && donationsRequested[limit].id;

  if (startId) {
    const startIndex = findIndex(donationsRequested, [
      "id",
      parseInt(startId)
    ]);

    const endIndex = startIndex + limit;
    nextId = donationsRequested[endIndex] && donationsRequested[endIndex].id;

    donationsRequested = donationsRequested.slice(startIndex, endIndex);
  } else {
    donationsRequested = donationsRequested.slice(0, limit);
  }

  res.send({
    value: donationsRequested,
    nextId: nextId
  });
};

module.exports = { getDonations }