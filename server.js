const jsonServer = require('json-server');
const express = require('express');
const fs = require('fs');
const path = require('path')
const donationRoutes = require('./custom-routes/donations')
const campaignsRoutes = require('./custom-routes/campaigns')
const photoRoutes = require('./custom-routes/photo')
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 3000;


server.use(middlewares);
server.use(jsonServer.bodyParser)
server.use(
  jsonServer.rewriter({
    '/api/v1/fund': '/api/v1/campaigns',
    '/api/v1/fund/:id': '/api/v1/campaigns/:id'
  })
);

const publicPath = path.join(process.cwd(), 'sample-images/');
server.use(express.static(publicPath, { maxAge: 31557600000 }));

server.post('/api/v1/photos', photoRoutes.postPhoto);
server.get('/api/v1/donations', donationRoutes.getDonations);
server.get('/api/v1/campaigns', campaignsRoutes.getCampaigns);
server.use('/api/v1/', router);

server.listen(port, () => {
  console.log(`json-server listening on ${port}`);
});
